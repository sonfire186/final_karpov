docker run -d -p 5000:5000 --restart=always --name registry registry:2

docker tag gateway 95.217.5.94:5000/gateway
docker push 95.217.5.94:5000/gateway

docker tag tf_serving_scam 95.217.5.94:5000/tf_serving_scam
docker push 95.217.5.94:5000/tf_serving_scam

docker tag index_service 95.217.5.94:5000/index_service
docker push 95.217.5.94:5000/index_service