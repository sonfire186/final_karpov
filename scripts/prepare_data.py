import numpy as np
import json
import faiss
import joblib
import os
import pickle

dim = 512

for dg in [1, 2]:

    with open(f'data/clusters_use_dg{dg}.json', encoding='UTF-8') as file:
        clusters = json.load(file)
    os.makedirs(f'dgs/dg_{dg}', exist_ok=True)

    for clust_id in range(4):
        os.makedirs(f'dgs/dg_{dg}/clust_{clust_id}', exist_ok=True)
        with open(f'dgs/dg_{dg}/clust_{clust_id}/sentences.json', 'w', encoding='UTF-8') as file:
            json.dump(clusters[str(clust_id)], file)

for dg in [1, 2]:

    with open(f'data/clusters_use_dg{dg}.json', encoding='UTF-8') as file:
        clusters = json.load(file)

    with open(f'data/use_embeddings_dg{dg}.pkl', 'rb') as file:
        use_embeddings = pickle.load(file)

    for clust_id in range(4):
        embs_cluster = np.vstack(
            [use_embeddings[clusters[str(clust_id)][index]] for index in
             range(len(clusters[str(clust_id)]))])
        index = faiss.IndexFlatL2(dim)
        index.add(embs_cluster)
        joblib.dump(index, f'dgs/dg_{dg}/clust_{clust_id}/index')

for dg in [1, 2]:
    with open(f'data/clusters_centers_use_dg{dg}.pkl', 'rb') as file:
        clusters_centers = pickle.load(file)

        clust_centers_m = np.array([clusters_centers[str(index)] for index in range(4)])
        np.save(f'dgs/dg_{dg}/clust_centers', clust_centers_m)
