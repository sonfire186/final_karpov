import json
import requests
import numpy as np
from flask import Flask, request, jsonify

clust_centers_m = np.load('/indices/clust_centers.npy')
path_tf_serving = 'http://host.docker.internal:8501/v1/models/use:predict'

app = Flask(__name__)


@app.route('/input', methods=['POST'])
def get_data():
    json_val = request.json
    query = json_val['query']

    data = {
        'inputs': [query],
    }
    rest_results = requests.post(path_tf_serving, json=data).json()

    input_emb = rest_results['outputs'][0]
    input_emb = np.array(input_emb)

    u_v = np.sum(input_emb * clust_centers_m, axis=1)
    abs_u = np.sqrt(np.sum(input_emb * input_emb))
    abs_v = np.sqrt(np.sum(clust_centers_m * clust_centers_m, axis=1))
    cos_sims = u_v / (abs_u * abs_v)
    input_cluster = np.argmin(cos_sims)

    resp = json.loads(
        requests.post(
            f'http://host.docker.internal:600{input_cluster}/get_best_from_k',
            json={'emb': input_emb.tolist()}
        ).text)

    match = resp['best_match']
    cluster = resp['cluster_id']

    return jsonify(best_match=match, cluster=cluster)


@app.route('/ping', methods=['GET'])
def get_ping():
    return 'pong'


if __name__ == '__main__':
    app.run()
