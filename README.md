# Final Karpov(tg - @SonFire)

Сервис состоит из нескольких сервисов:

- gateway - сервис отвечает за приемку запрос и перевод в эмбеддинги, с последующей отправкой на нужный класстер
- tf_serving_scam - сервис генерирует эмбеддинги
- index_service - сервис индексла кластеров

# Deploy

* Скопировать репозиторий на машину.(Модели для превого поколения находятся в репозитории, для загрузки моделей так же
  есть скрипт `delivery.sh`)
* Создать докер образы сервисов.(Все скрипты в папке scripts и также отдельные скрипты в папки сервисов)

``chmod +x build_container.sh & ./build_container.sh``

* Регистрация образов с созданием регистра.(Все адреса машины указаны статично)
  ``chmod +x docker_registry.sh & ./docker_registry.sh``
* Сборка сервисов ``docker stack deploy --compose-file docker-compose.yml qa`` (docker swarm должен быть предварительно
  настроен)

# Example
POST запрос на gateway

```
{
   "query": "karpov"
}

```

Ответ

```
{
    "best_match": "How did you get over a break up?",
    "cluster": "1"
}
```

# Контрольные вопросы
1. Точно ли вы подняли все, что нужно QA системе? Если вы закодили все внутри одного приложения, то это не зачтется :(

```
Да все сервисы подняты
- gateway - сервис отвечает за приемку запрос и перевод в эмбеддинги, с последующей отправкой на нужный класстер
- tf_serving_scam - сервис генерирует эмбеддинги
- index_service - сервис индексла кластеров
```

2. Каким образом файлы с индексами попадают на целевые машины? Попадают ли индексы адресно или происходит broadcast одного файла на все сервера?

`` Первые индексы лежат в git, дальше broadcast возможен``

3. Написали ли ли вы стратегию обновления индекса в системе? Что это за стратегия: Blue / Green или Rolling? А есть ли у вашей стратегии downtime?

`` Rolling update. downtime = 30sec ``

4. При обновлении индекса обновляются ли необходимые параметры в других частях системы?
Т.е. не случится ли такого, что gateway направляет запрос не на тот индекс если во время обновления что то пошло не так? Не забыли ли вы про идемпотентность обновлений в вашей стратегии?

`` Обновление бесшовное ``

5. Что будет, если индекс не сможет обновиться? (например, попался битый файл индекса)

`` Сервисы упадут(проверено) ``

6. Может быть, вы не поленились и предусмотрели какие-нибудь failover?

`` Не предусмотрел(мало времени было) ``

7. Вы использовали уже готовые инструменты, знакомые многим, или решили написали свой shellсипед?

`` shellсипед ``

8. А что с самими приложениями сервинга? Не перегружен ли его запуск внешними зависимостями, правильно ли выключается и поднимается приложение. Существует способ быстро проверить здорово ли приложение?

`` Написано инстукция по развертки и тестированию. На каждом сервисе есть рука PING ``