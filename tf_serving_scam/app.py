from flask import Flask, jsonify
import numpy as np

app = Flask(__name__)


@app.route('/v1/models/use:predict', methods=['POST'])
def get_model_predict():
    result = list(np.random.rand(512))

    return jsonify(outputs=[result])


@app.route('/ping', methods=['GET'])
def get_ping():
    return 'pong'


if __name__ == '__main__':
    app.run()
